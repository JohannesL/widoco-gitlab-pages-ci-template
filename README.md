# Widoco GitLab Pages CI Template

This template project demonstrates the use of [Widoco](https://github.com/dgarijo/Widoco) in a GitLab CI pipeline, publishing the results via GitLab pages. This example uses the FOAF ontology.

The resulting Widoco page is served via GitLab pages at https://johannesl.gitlab.io/widoco-gitlab-pages-ci-template.
